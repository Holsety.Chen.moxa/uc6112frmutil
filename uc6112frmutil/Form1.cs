﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using ICSharpCode.SharpZipLib.Tar;

namespace uc6112frmutil
{
    public partial class Form1 : Form
    {
        int maxProcessWorker = 50;
        string defaultRouteIp = "192.168.2.1";
        BackgroundWorker bwProcessWorker;
        string bypassVersion = "";
        string[] firmware;
        List<string> auth;
        BindingList<targetInfo> targetDevice;

        public class targetInfo
        {
            [System.ComponentModel.DisplayName("#")]
            public int number { set; get; }
            [System.ComponentModel.DisplayName("IP Address")]
            public string ipaddress { set; get; }
//            [System.ComponentModel.DisplayName("Current Version")]
//            public string version { set; get; }
            [System.ComponentModel.DisplayName("Description")]
            public string description { set; get; }
            public int status;
            public BackgroundWorker bwWorker;
        }

        public Form1()
        {
            InitializeComponent();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            this.Text += " - ";
            this.Text += fvi.FileVersion;

            Load +=new EventHandler(Form1_Load);
            SizeChanged += new EventHandler(Form1_SizeChanged);

            buttonStart.Click +=new EventHandler(buttonStart_Click);
            buttonAddIP.Click += new EventHandler(buttonAddIP_Click);
            buttonDelIP.Click += new EventHandler(buttonDelIP_Click);
            buttonClear.Click += new EventHandler(buttonClear_Click);
            buttonImportIP.Click += new EventHandler(buttonImportIP_Click);
            buttonLoadFirmware.Click += new EventHandler(buttonLoadFirmware_Click);

            bwProcessWorker = new BackgroundWorker();
            bwProcessWorker.WorkerReportsProgress = true;
            bwProcessWorker.ProgressChanged += new ProgressChangedEventHandler(bwProcessWorker_ProgressChanged);
            bwProcessWorker.DoWork += new DoWorkEventHandler(bwProcessWorker_DoWork);
            bwProcessWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwProcessWorker_RunWorkerCompleted);

            targetDevice = new BindingList<targetInfo>();
            dataGridView1.DataSource = targetDevice;
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }

            auth = new List<string>();
        }

        string CopyToMemoryStream(TarInputStream s, MemoryStream memoryStream)
        {
            byte[] dataBuff = new byte[4096];
            string md5hash = "";

            for (int i = 0; i < memoryStream.Capacity;)
            {
                int len = s.Read(dataBuff, 0, dataBuff.Length);
                memoryStream.Write(dataBuff, 0, len);
                i += len;
            }
            memoryStream.Seek(0, SeekOrigin.Begin);

            using (MD5 md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(memoryStream);
                md5hash = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
            memoryStream.Seek(0, SeekOrigin.Begin);

            return md5hash;
        }

        void buttonLoadFirmware_Click(object sender, EventArgs e)
        {
            if (!buttonStart.Enabled)
            {
                return;
            }

            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Multiselect = false;
            openfile.Title = "Select patch file";
            openfile.InitialDirectory = Directory.GetCurrentDirectory();
            openfile.Filter = "TAR|*.tar";
            if (DialogResult.OK != openfile.ShowDialog())
            {
                return;
            }

            textBoxFirmwarePath.Text = openfile.SafeFileName;

            //firmware = File.ReadAllLines(openfile.FileName);
            firmware = new string[0];

            try
            {
                string[] key = openfile.SafeFileName.Split('_');
                bypassVersion = key[1];
            }
            catch
            {
                bypassVersion = "";
            }
        }

        void updateCount()
        {
            int complete = 0;
            int fail = 0;

            foreach (targetInfo dev in targetDevice)
            {
                if (dev.status < 0)
                {
                    fail++;
                }
                else if (dev.status > 0)
                {
                    complete++;
                }
            }

            textBoxCount.Text = string.Format("{0} / {1} / {2}",
                targetDevice.Count, complete, fail);
        }

        void buttonImportIP_Click(object sender, EventArgs e)
        {
            if (!buttonStart.Enabled)
            {
                return;
            }

            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Multiselect = false;
            openfile.Title = "Import IP address";
            openfile.InitialDirectory = Directory.GetCurrentDirectory();
            openfile.Filter = "TXT|*.txt";
            if (DialogResult.OK != openfile.ShowDialog())
            {
                return;
            }

            string [] ipaddrs = File.ReadAllLines(openfile.FileName);

            foreach (string ipaddr in ipaddrs)
            {
                try
                {
                    IPAddress.Parse(ipaddr);
                }
                catch { continue; }

                bool found = false;
                foreach (targetInfo dev in targetDevice)
                {
                    if (dev.ipaddress == ipaddr)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                {
                    continue;
                }

                targetInfo target = new targetInfo();
                target.number = targetDevice.Count + 1;
                target.ipaddress = ipaddr;
                target.bwWorker = new BackgroundWorker();
                target.bwWorker.DoWork += new DoWorkEventHandler(bwWorker_DoWork);

                targetDevice.Add(target);
            }

            updateCount();
        }

        void buttonAddIP_Click(object sender, EventArgs e)
        {
            if (!buttonStart.Enabled)
            {
                return;
            }

            try
            {
                IPAddress.Parse(textBoxIpAddress.Text);
            }
            catch { return; }

            foreach (targetInfo dev in targetDevice)
            {
                if (dev.ipaddress == textBoxIpAddress.Text)
                    return;
            }

            targetInfo target = new targetInfo();
            target.number = targetDevice.Count + 1;
            target.ipaddress = textBoxIpAddress.Text;
            target.bwWorker = new BackgroundWorker();
            target.bwWorker.DoWork += new DoWorkEventHandler(bwWorker_DoWork);

            targetDevice.Add(target);

            updateCount();
        }

        void buttonClear_Click(object sender, EventArgs e)
        {
            if (!buttonStart.Enabled)
            {
                return;
            }

            foreach (targetInfo dev in targetDevice)
            {
                dev.bwWorker.Dispose();
                dev.bwWorker = null;
            }

            targetDevice.Clear();

            updateCount();
        }

        void buttonDelIP_Click(object sender, EventArgs e)
        {
            if (!buttonStart.Enabled)
            {
                return;
            }

            try
            {
                IPAddress.Parse(textBoxIpAddress.Text);
            }
            catch { return; }

            foreach (targetInfo dev in targetDevice)
            {
                if (dev.ipaddress == textBoxIpAddress.Text)
                {
                    dev.bwWorker.Dispose();
                    dev.bwWorker = null;
                    targetDevice.Remove(dev);
                    return;
                }
            }

            // re-assign number
            int i = 0;
            foreach (targetInfo dev in targetDevice)
            {
                dev.number = ++i;
            }

            updateCount();
        }

        void buttonStart_Click(object sender, EventArgs e)
        {
            if (targetDevice.Count <= 0)
            {
                return;
            }

            if (bwProcessWorker.IsBusy)
            {
                return;
            }

            if (firmware == null)
            {
                buttonLoadFirmware_Click(null, EventArgs.Empty);
            }

            auth.Clear();

            auth.Add("root;tpcroot"); // chem
            //auth.Add("root;tatungami"); // tatung

            if (!string.IsNullOrEmpty(textBoxAccount.Text) &&
                !string.IsNullOrEmpty(textBoxPassword.Text))
            {
                auth.Add(string.Format("{0};{1}", textBoxAccount.Text, textBoxPassword.Text));
            }

            buttonStart.Enabled = false;

            foreach (targetInfo dev in targetDevice)
            {
                dev.status = 0;
                dev.description = "";
                //dev.version = "";
            }

            bwProcessWorker.RunWorkerAsync();
        }

        void logfile(string filename, string message, bool isErr = false)
        {
            string logfolder = ".\\log\\";
            string logfile = logfolder + filename + ".log";

            if (isErr)
            {
                logfile = logfolder + filename + ".err";
            }

            if (!Directory.Exists(logfolder))
            {
                Directory.CreateDirectory(logfolder);
            }

            if (!File.Exists(logfile))
            {
                using (StreamWriter sw = File.CreateText(logfile))
                {
                }
            }

            using (StreamWriter sw = File.AppendText(logfile))
            {
                sw.Write(string.Format("[ {0:yyyy/MM/dd hh:mm:ss} ] ", DateTime.Now));
                sw.WriteLine(message);
            }
        }

        void UploadFile(string ip, string acc, string pw, string path, MemoryStream ms)
        {
            using (var sftpClient = new SftpClient(ip, acc, pw))
            {
                sftpClient.Connect(); // Line throwing the exception

                if (!sftpClient.IsConnected)
                {
                    throw new Exception("Connect sftp fail!");
                }

                if (sftpClient.Exists(path))
                    sftpClient.DeleteFile(path);
                SftpFileStream sftpFileStream = sftpClient.OpenWrite(path);
                byte[] buff = new byte[1024];
                while (true)
                {
                    int size = ms.Read(buff, 0, buff.Length);
                    if (size == 0)
                        break;
                    sftpFileStream.Write(buff, 0, size);
                    sftpFileStream.Flush();
                }
                sftpFileStream.Close();

                sftpClient.Disconnect();
            }
        }

        void bwWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;
            SshClient sshClient = null;
            SshCommand sshcmd;
            string[] keyparsing;
            targetInfo info = e.Argument as targetInfo;
            string routeIp = defaultRouteIp;
            string ipaddr = info.ipaddress;
            string acc = "";
            string pw = "";
            bool reqReboot = false;

            #region --- read uboot & kernel tar file to memory ---
            MemoryStream streamMtdPackage = null;
            MemoryStream streamDtb = null;
            MemoryStream streamDtbSpi = null;
            MemoryStream streamMlo = null;
            MemoryStream streamUboot = null;
            MemoryStream streamUimage = null;
            string hashDtb = "";
            string hashDtbSpi = "";
            string hashMlo = "";
            string hashUboot = "";
            string hashUimage = "";
            using (TarInputStream s = new TarInputStream(File.OpenRead(textBoxFirmwarePath.Text)))
            {
                TarEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    int size = Convert.ToInt32(theEntry.Size);
                    if (theEntry.Name.Contains("mtd-utils"))
                    {
                        streamMtdPackage = new MemoryStream(size);
                        CopyToMemoryStream(s, streamMtdPackage);
                    }
                    else if (theEntry.Name.Contains("moxa-uc6112.dtb"))
                    {
                        streamDtb = new MemoryStream(size);
                        hashDtb = CopyToMemoryStream(s, streamDtb);
                    }
                    else if (theEntry.Name.Contains("moxa-uc6112-spi.dtb"))
                    {
                        streamDtbSpi = new MemoryStream(size);
                        hashDtbSpi = CopyToMemoryStream(s, streamDtbSpi);
                    }
                    else if (theEntry.Name.Contains("MLO.byteswap"))
                    {
                        streamMlo = new MemoryStream(size);
                        hashMlo = CopyToMemoryStream(s, streamMlo); ;
                    }
                    else if (theEntry.Name.Contains("u-boot.img"))
                    {
                        streamUboot = new MemoryStream(Convert.ToInt32(theEntry.Size));
                        hashUboot = CopyToMemoryStream(s, streamUboot); ;
                    }
                    else if (theEntry.Name.Contains("uImage"))
                    {
                        streamUimage = new MemoryStream(Convert.ToInt32(theEntry.Size));
                        hashUimage = CopyToMemoryStream(s, streamUimage); ;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            #endregion

#if DEBUG
            Stopwatch sw = new Stopwatch();
            sw.Reset();
            sw.Start();
#endif

            try
            {
                #region --- Login method ---
                info.description = "Connect to target...";
                logfile(ipaddr, info.description);

                foreach (string str in auth)
                {
                    keyparsing = str.Split(';');
                    acc = keyparsing[0];
                    pw = keyparsing[1];

                    try
                    {
                        sshClient = new SshClient(ipaddr, acc, pw);
                        sshClient.Connect();
                        if (sshClient.IsConnected)
                        {
                            break;
                        }
                    }
                    catch
                    {
                        //sshClient.Dispose();
                        sshClient = null;
                    }
                }

                if (sshClient == null)
                {
                    throw new Exception("Connect fail!");
                }
                else
                {
                    if (!sshClient.IsConnected)
                    {
                        throw new Exception("Disconnect!");
                    }
                }

                sshcmd = sshClient.RunCommand("kversion");
                if (!sshcmd.Result.Contains("UC-6112"))
                    throw new Exception("It is not UC-6112!");

                #endregion

                #region --- Phase 0: Remove moxa test program ---
                info.description = "Phase 0: Remove moxa test program...";
                sshcmd = sshClient.RunCommand("cat /bin/factory_test.sh | grep CHEM");
                if (sshcmd.Result != "")
                {
                    sshClient.RunCommand("echo '#!/bin/sh' >/bin/factory_test.sh");
                    sshClient.RunCommand("mv /etc/rc.local.backup /etc/rc.local");
                    reqReboot = true;
                }
                #endregion

                #region --- Phase 1: Change DTB ---
                info.description = "Phase 1: Check and update DTB file...";
                logfile(ipaddr, info.description);

                sshClient.RunCommand("mount /dev/mmcblk0p1 /mnt");
                sshcmd = sshClient.RunCommand("md5sum /mnt/moxa-uc6112.dtb | awk '{print $1}'");
                if (!sshcmd.Result.Contains(hashDtbSpi))
                {
                    UploadFile(ipaddr, acc, pw, "/run/moxa-uc6112-spi.dtb", streamDtbSpi);
                    sshClient.RunCommand("cp /mnt/moxa-uc6112.dtb /mnt/moxa-uc6112.dtb.bak");
                    sshClient.RunCommand("cp /run/moxa-uc6112-spi.dtb /mnt/moxa-uc6112.dtb");
                    reqReboot = true;
                }
                else
                {
                    info.description = "Phase 1: DTB file no need to update...";
                    logfile(ipaddr, info.description);
                }
                sshClient.RunCommand("umount /mnt");

                if (reqReboot)
                {
                    info.description = "Phase 1: Reboot device and wait for reconnect";
                    logfile(ipaddr, info.description);

                    try
                    {
                        sshClient.RunCommand("reboot");
                        sshClient.Disconnect();
                    }
                    catch { }

                    sshClient.Dispose();

                    for (int i = 0; i < 30; i++)
                    {
                        System.Threading.Thread.Sleep(10000);
                        try
                        {
                            sshClient = new SshClient(ipaddr, acc, pw);
                            sshClient.Connect();
                            if (sshClient.IsConnected)
                                break;
                        }
                        catch
                        {
                            sshClient.Dispose();
                        }
                    }

                    if (!sshClient.IsConnected)
                    {
                        sshClient.Dispose();
                        throw new Exception("Phase 1: Disconnect!");
                    }

                    reqReboot = false;
                }
                #endregion

                #region --- Phase 2: Update U-Boot & Kernel ---
                //
                // check_mtd_device
                //
                sshcmd = sshClient.RunCommand("ls /dev/mtd*");
                if (sshcmd.Result == "")
                {
                    throw new Exception("There is no /dev/mtd device!");
                }

                //
                // check_mtd_utils
                //
                info.description = "Phase 2: Check & Install required package...";
                logfile(ipaddr, info.description);

                sshcmd = sshClient.RunCommand("dpkg -l mtd-utils | grep mtd-utils | awk '{print $1}'");
                if (!sshcmd.Result.Contains("ii"))
                {
                    UploadFile(ipaddr, acc, pw, "/run/mtd-utils_armhf.deb", streamMtdPackage);
                    sshClient.RunCommand("dpkg -i /run/mtd-utils_armhf.deb");
                    sshcmd = sshClient.RunCommand("dpkg -l mtd-utils | grep mtd-utils | awk '{print $1}'");
                    if (!sshcmd.Result.Contains("ii"))
                    {
                        throw new Exception("mtd-utils not found!");
                    }
                }

                //
                // check & update mlo
                //
                info.description = "Phase 2: Check & update mlo file...";
                logfile(ipaddr, info.description);

                sshcmd = sshClient.RunCommand(string.Format("dd if=/dev/mtd0 of=/run/mlo2 count={0}", (streamMlo.Length / 512) + 1));
                sshcmd = sshClient.RunCommand(string.Format("truncate --size={0} /run/mlo2", streamMlo.Length));
                sshcmd = sshClient.RunCommand("md5sum /run/mlo2 | awk '{print $1}'");
                if (!sshcmd.Result.Contains(hashMlo))
                {
                    UploadFile(ipaddr, acc, pw, "/run/mlo", streamMlo);
                    sshClient.RunCommand("flash_erase /dev/mtd0 0 8");
                    sshClient.RunCommand("flashcp /run/mlo /dev/mtd0");
                }

                //
                // check & update uboot
                //
                info.description = "Phase 2: Check & update uboot...";
                logfile(ipaddr, info.description);
                sshcmd = sshClient.RunCommand(string.Format("dd if=/dev/mtd1 of=/run/uboot2 count={0}", (streamUboot.Length / 512) + 1));
                sshcmd = sshClient.RunCommand(string.Format("truncate --size={0} /run/uboot2", streamUboot.Length));
                sshcmd = sshClient.RunCommand("md5sum /run/uboot2 | awk '{print $1}'");
                if (!sshcmd.Result.Contains(hashUboot))
                {
                    UploadFile(ipaddr, acc, pw, "/run/uboot", streamUboot);
                    sshClient.RunCommand("flash_erase /dev/mtd1 0 16");
                    sshClient.RunCommand("flashcp /run/uboot /dev/mtd1");
                }

                //
                // check & update kernel
                //
                info.description = "Phase 2: Check & update kernel...";
                logfile(ipaddr, info.description);
                sshClient.RunCommand("mount /dev/mmcblk0p1 /mnt");
                sshcmd = sshClient.RunCommand("md5sum /mnt/uImage | awk '{print $1}'");
                if (!sshcmd.Result.Contains(hashUimage))
                {
                    UploadFile(ipaddr, acc, pw, "/run/uImage", streamUimage);
                    sshClient.RunCommand("cp /mnt/uImage /mnt/uImage.bak");
                    sshClient.RunCommand("cp /run/uImage /mnt");
                    sshClient.RunCommand("sync");
                    reqReboot = true;
                }
                sshClient.RunCommand("umount /mnt");

                if (reqReboot)
                {
                    info.description = "Phase 2: Reboot device and wait for reconnect";
                    logfile(ipaddr, info.description);

                    try
                    {
                        sshClient.RunCommand("reboot");
                        sshClient.Disconnect();
                    }
                    catch { }

                    sshClient.Dispose();

                    for (int i = 0; i < 30; i++)
                    {
                        System.Threading.Thread.Sleep(10000);
                        try
                        {
                            sshClient = new SshClient(ipaddr, acc, pw);
                            sshClient.Connect();
                            if (sshClient.IsConnected)
                                break;
                        }
                        catch
                        {
                            sshClient.Dispose();
                        }
                    }

                    if (!sshClient.IsConnected)
                    {
                        sshClient.Dispose();
                        throw new Exception("Phase 2: Disconnect!");
                    }

                    reqReboot = false;
                }
                #endregion

                #region --- Phase 3: update reset to default partition ---
                info.description = "Phase 3: Update reset to default partition...";
                logfile(ipaddr, info.description);

                string md5hash = "";

                using (FileStream fs = new FileStream("setdef", FileMode.Open))
                {
                    using (MD5 md5 = MD5.Create())
                    {
                        var hash = md5.ComputeHash(fs);
                        md5hash = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                    }
                }

                sshcmd = sshClient.RunCommand("md5sum /sbin/setdef | awk '{print $1}'");
                if (!sshcmd.Result.Contains(md5hash))
                {
                    using (var sftpClient = new SftpClient(ipaddr, acc, pw))
                    {
                        sftpClient.Connect(); // Line throwing the exception

                        if (!sftpClient.IsConnected)
                        {
                            throw new Exception("Connect sftp fail!");
                        }

                        using (FileStream fs = new FileStream("setdef", FileMode.Open))
                        {
                            sftpClient.UploadFile(fs, "/sbin/setdef");
                        }
                        sftpClient.Disconnect();
                    }
                    sshClient.RunCommand("sync");
                }
                else
                {
                    info.description = "Phase 3: Reset to default binary no need to update...";
                    logfile(ipaddr, info.description);
                }

                md5hash = "";
                using (FileStream fs = new FileStream("setdef-chroot", FileMode.Open))
                {
                    using (MD5 md5 = MD5.Create())
                    {
                        var hash = md5.ComputeHash(fs);
                        md5hash = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                    }

                }
                sshClient.RunCommand("mkdir /var/run/p3");
                sshClient.RunCommand("mount -t ext4 /dev/mmcblk0p3 /var/run/p3");
                sshcmd = sshClient.RunCommand("md5sum /var/run/p3/setdef-chroot | awk '{print $1}'");
                if (!sshcmd.Result.Contains(md5hash))
                {
                    using (var sftpClient = new SftpClient(ipaddr, acc, pw))
                    {
                        sftpClient.Connect(); // Line throwing the exception

                        if (!sftpClient.IsConnected)
                        {
                            throw new Exception("Connect sftp fail!");
                        }

                        using (FileStream fs = new FileStream("setdef-chroot", FileMode.Open))
                        {
                            sftpClient.UploadFile(fs, "/var/run/p3/setdef-chroot");
                        }
                        sftpClient.Disconnect();
                    }
                    sshClient.RunCommand("sync");
                }
                else
                {
                    info.description = "Phase 3: Reset to default partition no need to update...";
                    logfile(ipaddr, info.description);
                }

                //
                // Update hwclock.tar.gz to p3
                //
                md5hash = "";
                using (FileStream fs = new FileStream("hwclock.tar.gz", FileMode.Open))
                {
                    using (MD5 md5 = MD5.Create())
                    {
                        var hash = md5.ComputeHash(fs);
                        md5hash = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                    }

                }
                sshcmd = sshClient.RunCommand("md5sum /var/run/p3/hwclock.tar.gz | awk '{print $1}'");
                if (!sshcmd.Result.Contains(md5hash))
                {
                    using (var sftpClient = new SftpClient(ipaddr, acc, pw))
                    {
                        sftpClient.Connect(); // Line throwing the exception

                        if (!sftpClient.IsConnected)
                        {
                            throw new Exception("Connect sftp fail!");
                        }

                        using (FileStream fs = new FileStream("hwclock.tar.gz", FileMode.Open))
                        {
                            sftpClient.UploadFile(fs, "/var/run/p3/hwclock.tar.gz");
                        }
                        sftpClient.Disconnect();
                    }
                    sshClient.RunCommand("sync");
                }
                else
                {
                    info.description = "Phase 3: Reset to default partition(hwclock) no need to update...";
                    logfile(ipaddr, info.description);
                }

                sshClient.RunCommand("umount /var/run/p3");
                sshClient.RunCommand("rmdir /var/run/p3");
                #endregion

                #region --- Phase 4: hwclock patch ---
                //
                // Reason: old kernel has watchdog bug so original hwclock should be wrapper,
                //         and new kernel fixed. So need to remove wrapper
                //
                info.description = "Phase 4: sbin hwclock patch...";
                logfile(ipaddr, info.description);
                sshcmd = sshClient.RunCommand("diff -q /sbin/_hwclock /sbin/hwclock");
				if (sshcmd.ExitStatus != 0)
                {
                    sshClient.RunCommand("cp /sbin/_hwclock /sbin/hwclock");
                    sshClient.RunCommand("sync");
                }
                else
                {
                    info.description = "Phase 4: No need to patch sbin hwclock...";
                    logfile(ipaddr, info.description);
                }

                info.description = "Phase 4-1: bin hwclock patch...";
                logfile(ipaddr, info.description);
                sshcmd = sshClient.RunCommand("diff -q /bin/_hwclock /bin/hwclock");
                if (sshcmd.ExitStatus != 0)
                {
                    sshClient.RunCommand("cp /bin/_hwclock /bin/hwclock");
                    sshClient.RunCommand("sync");
                }
                else
                {
                    info.description = "Phase 4-1: No need to patch bin hwclock...";
                    logfile(ipaddr, info.description);
                }

                #endregion

                info.description = "Check and update complete!";
                logfile(ipaddr, info.description);

                info.status = 1;
            }
            catch (Exception ex)
            {
                info.status = -1;
                info.description = ex.Message;
                logfile(ipaddr, ex.Message);
                logfile(ipaddr, ex.ToString(), true);
            }

            if (sshClient != null)
            {
                sshClient.Disconnect();
                sshClient.Dispose();
                sshClient = null;
            }

#if DEBUG
            Console.WriteLine(String.Format("Cost {0} Second", sw.ElapsedMilliseconds/1000));
            sw.Stop();
#endif
        }

        void bwProcessWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            targetDevice.ResetBindings();
            updateCount();
        }

        void bwProcessWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            targetDevice.ResetBindings();
            updateCount();
            buttonStart.Enabled = true;

            List<string> ips = new List<string>();
            foreach (targetInfo dev in targetDevice)
            {
                if (dev.status < 0)
                {
                    ips.Add(dev.ipaddress);
                }
            }

            if (ips.Count > 0)
            {
                if (DialogResult.OK == MessageBox.Show(
                    "There are some devces update fail.\nExport to file?",
                    "",
                    MessageBoxButtons.OKCancel))
                {
                    SaveFileDialog savefile = new SaveFileDialog();
                    savefile.Title = "Export IP address";
                    savefile.InitialDirectory = Directory.GetCurrentDirectory();
                    savefile.Filter = "TXT|*.txt";
                    if (DialogResult.OK == savefile.ShowDialog())
                    {
                        StreamWriter sw = File.CreateText(savefile.FileName);
                        foreach(string ip in ips)
                        {
                            sw.WriteLine(ip);
                        }
                        sw.Close();
                        sw.Dispose();
                    }
                }
            }
        }

        void bwProcessWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;

            while (true)
            {
                int busyCount = 0;
                foreach (targetInfo dev in targetDevice)
                {
                    if (dev.bwWorker.IsBusy)
                    {
                        busyCount++;
                    }
                    else
                    {
                        if (busyCount > maxProcessWorker)
                        {
                            break;
                        }

                        if (dev.status == 0)
                        {
                            dev.bwWorker.RunWorkerAsync(dev);
                            busyCount++;
                        }
                    }
                }

                if (busyCount == 0)
                {
                    break;
                }

                bw.ReportProgress(0);
                Thread.Sleep(1000);
            }
        }

        void Form1_Load(object sender, EventArgs e)
        {
        }

        void Form1_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                Size size = this.Size;
                size.Height -= 120;
                size.Width -= 40;
                dataGridView1.Size = size;
            }
            catch
            {
            }
        }

    }
}
