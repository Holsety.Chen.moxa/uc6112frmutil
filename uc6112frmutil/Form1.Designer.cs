﻿namespace uc6112frmutil
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.textBoxIpAddress = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonLoadFirmware = new System.Windows.Forms.Button();
            this.textBoxFirmwarePath = new System.Windows.Forms.TextBox();
            this.buttonImportIP = new System.Windows.Forms.Button();
            this.buttonAddIP = new System.Windows.Forms.Button();
            this.labelAccount = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxAccount = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonDelIP = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(699, 9);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 44);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            // 
            // textBoxIpAddress
            // 
            this.textBoxIpAddress.Location = new System.Drawing.Point(152, 42);
            this.textBoxIpAddress.Name = "textBoxIpAddress";
            this.textBoxIpAddress.Size = new System.Drawing.Size(100, 20);
            this.textBoxIpAddress.TabIndex = 1;
            this.textBoxIpAddress.Text = "192.168.2.127";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 69);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(762, 214);
            this.dataGridView1.TabIndex = 2;
            // 
            // buttonLoadFirmware
            // 
            this.buttonLoadFirmware.Location = new System.Drawing.Point(13, 11);
            this.buttonLoadFirmware.Name = "buttonLoadFirmware";
            this.buttonLoadFirmware.Size = new System.Drawing.Size(94, 23);
            this.buttonLoadFirmware.TabIndex = 3;
            this.buttonLoadFirmware.Text = "Patch File";
            this.buttonLoadFirmware.UseVisualStyleBackColor = true;
            // 
            // textBoxFirmwarePath
            // 
            this.textBoxFirmwarePath.Location = new System.Drawing.Point(113, 13);
            this.textBoxFirmwarePath.Name = "textBoxFirmwarePath";
            this.textBoxFirmwarePath.ReadOnly = true;
            this.textBoxFirmwarePath.Size = new System.Drawing.Size(303, 20);
            this.textBoxFirmwarePath.TabIndex = 4;
            // 
            // buttonImportIP
            // 
            this.buttonImportIP.Location = new System.Drawing.Point(13, 40);
            this.buttonImportIP.Name = "buttonImportIP";
            this.buttonImportIP.Size = new System.Drawing.Size(75, 23);
            this.buttonImportIP.TabIndex = 5;
            this.buttonImportIP.Text = "Import IP";
            this.buttonImportIP.UseVisualStyleBackColor = true;
            // 
            // buttonAddIP
            // 
            this.buttonAddIP.Location = new System.Drawing.Point(94, 40);
            this.buttonAddIP.Name = "buttonAddIP";
            this.buttonAddIP.Size = new System.Drawing.Size(52, 23);
            this.buttonAddIP.TabIndex = 6;
            this.buttonAddIP.Text = "Add IP";
            this.buttonAddIP.UseVisualStyleBackColor = true;
            // 
            // labelAccount
            // 
            this.labelAccount.AutoSize = true;
            this.labelAccount.Location = new System.Drawing.Point(556, 13);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(31, 13);
            this.labelAccount.TabIndex = 7;
            this.labelAccount.Text = "ACC:";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(559, 39);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(28, 13);
            this.labelPassword.TabIndex = 8;
            this.labelPassword.Text = "PW:";
            // 
            // textBoxAccount
            // 
            this.textBoxAccount.Location = new System.Drawing.Point(593, 9);
            this.textBoxAccount.Name = "textBoxAccount";
            this.textBoxAccount.Size = new System.Drawing.Size(100, 20);
            this.textBoxAccount.TabIndex = 9;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(593, 35);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 10;
            // 
            // buttonDelIP
            // 
            this.buttonDelIP.Location = new System.Drawing.Point(258, 40);
            this.buttonDelIP.Name = "buttonDelIP";
            this.buttonDelIP.Size = new System.Drawing.Size(52, 23);
            this.buttonDelIP.TabIndex = 11;
            this.buttonDelIP.Text = "Del IP";
            this.buttonDelIP.UseVisualStyleBackColor = true;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(341, 39);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 12;
            this.buttonClear.Text = "Clear All";
            this.buttonClear.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(422, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Total / Complete / Fail";
            // 
            // textBoxCount
            // 
            this.textBoxCount.Location = new System.Drawing.Point(435, 35);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.ReadOnly = true;
            this.textBoxCount.Size = new System.Drawing.Size(100, 20);
            this.textBoxCount.TabIndex = 14;
            this.textBoxCount.Text = "0 / 0 / 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 295);
            this.Controls.Add(this.textBoxCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonDelIP);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxAccount);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelAccount);
            this.Controls.Add(this.buttonAddIP);
            this.Controls.Add(this.buttonImportIP);
            this.Controls.Add(this.textBoxFirmwarePath);
            this.Controls.Add(this.buttonLoadFirmware);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBoxIpAddress);
            this.Controls.Add(this.buttonStart);
            this.Name = "Form1";
            this.Text = "Moxa UC6112 firmware utility";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox textBoxIpAddress;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonLoadFirmware;
        private System.Windows.Forms.TextBox textBoxFirmwarePath;
        private System.Windows.Forms.Button buttonImportIP;
        private System.Windows.Forms.Button buttonAddIP;
        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxAccount;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonDelIP;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxCount;
    }
}

