﻿using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
#if DEBUG
using System.Runtime.InteropServices;
#endif

namespace uc6112frmutil
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            using (Mutex mutex = new Mutex(false, "Global\\" + assembly.GetType().GUID))
            {
#if DEBUG
                AllocConsole();
#endif

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());

#if DEBUG
                Console.WriteLine("Press Enter to exit...");
                Console.ReadLine();
                FreeConsole();
#endif
            }
        }

#if DEBUG
        [DllImport("kernel32.dll")]
        public static extern Boolean AllocConsole();
        [DllImport("kernel32.dll")]
        public static extern Boolean FreeConsole();
#endif
    }
}
